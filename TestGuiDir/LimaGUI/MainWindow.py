# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created: Tue May  6 07:27:15 2014
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

#from taurus.qt.qtgui.panel import TaurusDevicePanel
from TaurusDevicePanel import TaurusDevicePanel
from PyQt4 import QtCore, QtGui



try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Form(object):
    def setupUi(self, Form, args):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(780, 760)
        self.horizontalLayout = QtGui.QHBoxLayout(Form)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))

        self.tabWidget = QtGui.QTabWidget()


        self.tab1Scroll = QtGui.QScrollArea()
        self.tab2Scroll = QtGui.QScrollArea()
        self.tab3Scroll = QtGui.QScrollArea()
        self.tab4Scroll = QtGui.QScrollArea()

        self.tab1 = QtGui.QWidget(self.tab1Scroll)
        self.tab2 = QtGui.QWidget(self.tab2Scroll)
        self.tab3 = QtGui.QWidget(self.tab3Scroll)
        self.tab4 = QtGui.QWidget(self.tab4Scroll)

        self.tab1Layout = QtGui.QVBoxLayout(self.tab1)
        self.tab2Layout = QtGui.QVBoxLayout(self.tab2)
        self.tab3Layout = QtGui.QVBoxLayout(self.tab3)
        self.tab4Layout = QtGui.QVBoxLayout(self.tab4)

        self.tabWidget.addTab(self.tab1Scroll, "Camera Configuration")
        self.tabWidget.addTab(self.tab2Scroll, "Camera Calibration 1")
        self.tabWidget.addTab(self.tab3Scroll, "Camera Calibration 2")
        self.tabWidget.addTab(self.tab4Scroll, "Camera Monitoring")
        self.horizontalLayout.addWidget(self.tabWidget)

        self.limaCCDs = TaurusDevicePanel()
        self.roi2Spectrum = TaurusDevicePanel()
        self.flatField = TaurusDevicePanel()
        self.background = TaurusDevicePanel()
        self.roiCounter = TaurusDevicePanel()
        self.mask = TaurusDevicePanel()
        self.liveViewer = TaurusDevicePanel()

        self.limaCCDs.setModel(args[0])
        self.roi2Spectrum.setModel(args[1])
        self.flatField.setModel(args[2])
        self.background.setModel(args[3])
        self.roiCounter.setModel(args[4])
        self.mask.setModel(args[5])
        self.liveViewer.setModel(args[6])



        self.tab1Layout.addWidget(self.limaCCDs)
        self.tab1Scroll.setWidget(self.tab1)
        self.tab1Scroll.setWidgetResizable(True)

        self.tab2Layout.addWidget(self.roi2Spectrum)
        self.tab2Layout.addWidget(self.roiCounter)
        self.tab2Scroll.setWidget(self.tab2)
        self.tab2Scroll.setWidgetResizable(True)

        self.tab3Layout.addWidget(self.flatField)
        self.tab3Layout.addWidget(self.background)
        self.tab3Layout.addWidget(self.mask)
        self.tab3Scroll.setWidget(self.tab3)
        self.tab3Scroll.setWidgetResizable(True)

        self.tab4Layout.addWidget(self.liveViewer)
        self.tab4Scroll.setWidget(self.tab4)
        self.tab4Scroll.setWidgetResizable(True)


        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Lima GUI", "Lima GUI", None))

